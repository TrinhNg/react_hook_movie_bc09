import { Button, Container, TextField, Typography } from "@material-ui/core";
import React from "react";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import * as yup from "yup";
import { useHistory } from "react-router";
import { fetchLogin } from "../../Store/Action/Login";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  title: {
    color: "#BC63D1",
    margin: "25px 0",
    fontFamily: "Apple Color Emoji",
  },
});

const schema = yup.object().shape({
  taiKhoan: yup.string().required("User is required"),
  matKhau: yup.string().required("Pass is required"),
});

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSumit = (e) => {
    e.preventDefault();
    if (!formik.isValid) return;
    dispatch(
      fetchLogin(formik.values, () => {
        history.push("/dashboard");
      })
    );
  };

  const { title } = useStyles();
  return (
    <div>
      <Container maxWidth="sm">
        <Typography variant="h3" align="center" className={title}>
          Đăng Nhập
        </Typography>
        <form onSubmit={handleSumit}>
          <div style={{ marginBottom: 30 }}>
            <TextField
              name="taiKhoan"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.taiKhoan}
              fullWidth
              label="Tài khoản"
              variant="outlined"
            />
            {formik.touched.taiKhoan && (
              <p style={{ color: "red" }}>{formik.errors.taiKhoan}</p>
            )}
          </div>
          <div style={{ marginBottom: 30 }}>
            <TextField
              name="matKhau"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.matKhau}
              type="password"
              fullWidth
              label="Mật khẩu"
              variant="outlined"
            />
            {formik.touched.matKhau && (
              <p style={{ color: "red" }}>{formik.errors.matKhau}</p>
            )}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <Button type="submit" variant="contained" color="primary">
              Đăng Nhập
            </Button>
          </div>
        </form>
      </Container>
    </div>
  );
};

export default Login;
