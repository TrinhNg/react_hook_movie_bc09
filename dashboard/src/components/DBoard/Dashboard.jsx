import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useHistory } from "react-router";
import { Box, Typography } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { fetchDelUser } from "../../Store/Action/Dashboard";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  title: {
    color: "#BC63D1",
    margin: "25px 0",
    fontFamily: "Apple Color Emoji",
  },
  pagi: {
    margin: "25px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  th: {
    fontWeight: "bolder",
    fontSize: 20,
    color: "#B08BB8",
  },
  page: {
    backgroundColor: "#D7F3A6",
  },
  span: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 12,
  },
});

const DashBoard = ({ user, setUsePage }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  return (
    <div>
      <Box display="flex" alignItems="center" justifyContent="space-evenly">
        <Typography
          variant="h3"
          align="center"
          style={{ paddingLeft: "15%" }}
          className={classes.title}
        >
          DANH SÁCH NGƯỜI DÙNG
        </Typography>
        <Fab
          color="secondary"
          aria-label="add"
          onClick={() => {
            history.push("/add");
          }}
        >
          <AddIcon />
        </Fab>
      </Box>
      <TableContainer className={classes.page} component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center" className={classes.th}>
                Số thứ tự
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Tài khoản
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Mật khẩu
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Số điện thoại
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Email
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Mã nhóm
              </TableCell>
              <TableCell align="center" className={classes.th}>
                Chỉnh sửa
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user?.items?.map((item, index) => (
              <TableRow key={index}>
                <TableCell align="center">{index + 1}</TableCell>
                <TableCell align="center">{item.taiKhoan}</TableCell>
                <TableCell align="center">{item.matKhau}</TableCell>
                {(item.soDT === "") & (item.soDT === null) ? (
                  <TableCell align="center">{item.soDT}</TableCell>
                ) : (
                  <span className={classes.span}>Đang cập nhật</span>
                )}
                {/* <TableCell align="center">{item.soDT}</TableCell> */}
                <TableCell align="center">{item.email}</TableCell>
                <TableCell align="center">{item.maLoaiNguoiDung}</TableCell>
                <TableCell align="center">
                  <Button
                    variant="contained"
                    color="primary"
                    style={{ marginRight: 10 }}
                    onClick={() => {
                      history.push(`/edit/${item.taiKhoan}`);
                    }}
                  >
                    <EditIcon />
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      if (dispatch(fetchDelUser(item.taiKhoan))) {
                        alert("Xóa thành công");
                        window.location.reload();
                      }
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Pagination
        className={classes.pagi}
        variant="outlined"
        color="secondary"
        count={user?.totalPages}
        onChange={(e) => setUsePage(e.target.textContent)}
      />
    </div>
  );
};

export default DashBoard;
