import { type } from "../../Action/Style";

const User = {
  user: {},
};
const reducer = (state = User, action) => {
  switch (action.type) {
    case type.GET_ME:
      state.user = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};
export default reducer;
