// import { configCore } from "../../../core/configCore";
import { type } from "../../Action/Style";

const movie = {
  listMovie: [],
  movieId: {},
};
const reducer = (state = movie, action) => {
  switch (action.type) {
    case type.SET_MOVIE:
      state.listMovie = action.payload;
      return { ...state };
    case type.SET_MOVIE_ID:
      state.movieId = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};
export default reducer;
