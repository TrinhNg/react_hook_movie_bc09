import axios from "axios";
import { createAction } from "./create";
import { type } from "./Style";

export const fetchMovied = (page) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "GET",
        url: `http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP01&soTrang=${page}&soPhanTuTrenTrang=15`,
      });
      console.log("data", res.data.content);
      dispatch(createAction(type.SET_MOVIE, res.data.content));
    } catch (error) {
      console.log(error);
    }
  };
};
export const fetchMovieId = (id) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "GET",
        url: `http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      });
      dispatch(createAction(type.SET_MOVIE_ID, res.data.content));
      console.log("res", res);
    } catch (err) {
      console.log(err);
    }
  };
};
