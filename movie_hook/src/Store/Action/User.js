import axios from "axios";
import { createAction } from "./create";
import { type } from "./Style";

export const fetchUser = (value, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangKy",
        data: value,
      });
      callback();
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchSignIn = (value, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        data: value,
      });
      callback();
      localStorage.setItem("t", res.data.content.accessToken);
      dispatch();
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await axios({
      method: "POST",
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("t"),
      },
    });
    console.log("token", res);
    dispatch(createAction(type.GET_ME, res.data.content));
  } catch (error) {
    console.log(error);
  }
};
