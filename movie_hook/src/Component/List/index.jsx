import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
  CardActions,
  Grid,
  Container,
} from "@material-ui/core";
import React from "react";
import { NavLink } from "react-router-dom";
import Pagination from "@material-ui/lab/Pagination";
import useStyle from "./style";
import clsx from "clsx";

const List = ({ movies, setId }) => {
  console.log("item", movies);
  const { card, page } = useStyle();
  const test = clsx(card, page);
  return (
    <Container>
      <Grid container spacing={2} className={card}>
        {movies?.items?.map((film) => {
          return (
            <Grid item xs={6} sm={4}>
              <Card style={{ marginBottom: 20 }}>
                <CardActionArea>
                  <CardMedia
                    image={film.hinhAnh}
                    title="Contemplative Reptile"
                    style={{ height: 200 }}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      {film.tenPhim}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      title={film.moTa}
                    >
                      {film.moTa.substr(0, 60) + "..."}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <NavLink
                    to={`/detail/${film.maPhim}`}
                    style={{ textDecoration: "none" }}
                  >
                    <Button size="small" variant="outlined" color="secondary">
                      Chi tiết
                    </Button>
                  </NavLink>
                </CardActions>
              </Card>
            </Grid>
          );
        })}
        <Pagination
          count={movies?.totalPages}
          onChange={(e) => setId(e.target.textContent)}
          className={test}
        />
      </Grid>
    </Container>
  );
};

export default List;
