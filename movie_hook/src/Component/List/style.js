import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles(() => {
  return {
    title: {
      marginBottom: 20,
      color: "#67AA6C",
      fontFamily: "Raleway",
    },
    card: {
      maxWidth: 1280,
      margin: "auto",
    },
    page: {
      marginBottom: 20,
    },
  };
});
export default useStyle;
