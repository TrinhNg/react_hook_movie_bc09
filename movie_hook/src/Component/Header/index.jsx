import React from "react";
import { NavLink } from "react-router-dom";
import MovieFilterIcon from "@material-ui/icons/MovieFilter";
import SearchIcon from "@material-ui/icons/Search";
import { alpha, makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  InputBase,
  Button,
} from "@material-ui/core";
import { useSelector } from "react-redux";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => {
  return {
    header: {
      marginBottom: "2%",
    },
    backgournd: {
      backgroundColor: "#81C784",
    },
    link: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    navlink: {
      marginLeft: theme.spacing(2),
      color: "#b7ffbd",
      fontWeight: "bold",
      fontFamily: "Raleway",
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: alpha(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(5),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        minWidth: "22ch",
      },
    },
  };
});
const Header = () => {
  const history = useHistory();
  const token = localStorage.getItem("t");
  console.log(token);
  const style = useStyles();

  const user = useSelector((state) => {
    return state.User.user;
  });

  console.log("user", user);
  return (
    <AppBar position="static" className={style.header}>
      <Toolbar className={style.backgournd}>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <MovieFilterIcon />
        </IconButton>
        <Typography variant="h6">Movies</Typography>

        <div className={style.search}>
          <div className={style.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            classes={{
              root: style.inputRoot,
              input: style.inputInput,
            }}
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
          />
        </div>
        <div className={style.link} style={{ marginLeft: "58%" }}>
          {token ? (
            <>
              <IconButton>
                <AccountCircleIcon
                  titleAccess="Profile"
                  onClick={() => {
                    history.push("/profile");
                  }}
                />
              </IconButton>
              <Typography>Hi,{user.taiKhoan} </Typography>
              <Button
                color="primary"
                onClick={() => {
                  localStorage.removeItem("t");
                  history.push("/");
                  window.location.reload();
                }}
              >
                Log out
              </Button>
            </>
          ) : (
            <>
              <NavLink
                className={style.navlink}
                exact
                activeClassName={style.backgournd}
                to="/"
              >
                HOME
              </NavLink>
              <NavLink
                className={style.navlink}
                activeClassName={style.backgournd}
                to="/signin"
              >
                SIGN IN
              </NavLink>
              <NavLink
                className={style.navlink}
                activeClassName={style.backgournd}
                to="/signup"
              >
                SIGN UP
              </NavLink>
            </>
          )}
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
