import { Typography } from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../Component/Header/index";
import { fetchMovied } from "../../Store/Action/Movie";
import List from "../../Component/List";
import useStyle from "./style";

const Home = () => {
  const { title } = useStyle();
  const dispatch = useDispatch();

  const movies = useSelector((state) => {
    return state.Movie.listMovie;
  });

  const [id, setId] = useState("1");

  const fetchMovie = useCallback(() => {
    dispatch(fetchMovied(id));
  }, [id, dispatch]);

  useEffect(() => {
    fetchMovie();
  }, [id, dispatch]);

  return (
    <div>
      <Header />
      <Typography component="h1" variant="h3" align="center" className={title}>
        DANH SÁCH PHIM
      </Typography>
      <List movies={movies} setId={setId} />
    </div>
  );
};

export default Home;
