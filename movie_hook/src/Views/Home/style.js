import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles(() => {
  return {
    title: {
      marginBottom: 20,
      color: "#67AA6C",
      fontFamily: "Raleway",
    },
  };
});
export default useStyle;
