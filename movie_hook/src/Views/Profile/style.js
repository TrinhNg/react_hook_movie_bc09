import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles((theme) => {
  return {
    h3: {
      color: "#67AA6C",
      textAlign: "center",
      fontFamily: "Raleway",
      marginBottom: 30,
    },
    container: {
      width: "65%",
      margin: "auto",
      marginRight: theme.spacing(7),
    },
    avatar: {
      width: theme.spacing(15),
      height: theme.spacing(15),
      marginLeft: theme.spacing(4),
    },
    line: {
      borderRight: "1px solid #67AA6C",
      marginRight: 50,
    },
    info: {
      maxWidth: "55%",
    },
    title: {
      color: "#E885B3",
      marginBottom: theme.spacing(0.7),
      fontStyle: "italic",
    },
    span: {
      color: "#5E775D",
    },
    space: {
      marginBottom: 20,
      fontSize: theme.spacing(3),
    },
  };
});
export default useStyle;
