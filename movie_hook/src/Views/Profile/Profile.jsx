import { Avatar, Box, Grid, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import Header from "../../Component/Header";
import useStyle from "./style";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { withStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";

const StyledRating = withStyles({
  iconFilled: {
    color: "#ff3d47",
  },
  iconHover: {
    color: "#67AA6C",
  },
})(Rating);

const Profile = () => {
  const user = useSelector((state) => {
    return state.User.user;
  });

  const style = useStyle();

  return (
    <div>
      <Header />
      <Typography variant="h3" className={style.h3}>
        THÔNG TIN NGƯỜI DÙNG
      </Typography>
      <Grid container spacing={1} className={style.container}>
        <Grid item xs="3" className={style.line}>
          <Avatar
            src="https://picsum.photos/seed/picsum/200"
            className={style.avatar}
          >
            {user.hoTen}
          </Avatar>
          <Box
            component="fieldset"
            mb={3}
            borderColor="transparent"
            marginTop="50px"
            left="50px"
          >
            <Typography component="legend" align="left">
              Current satisfaction
            </Typography>
            <StyledRating
              name="customized-color"
              defaultValue={2}
              getLabelText={(value) =>
                `${value} Heart${value !== 1 ? "s" : ""}`
              }
              precision={0.5}
              icon={<FavoriteIcon fontSize="inherit" />}
            />
          </Box>
        </Grid>
        <Grid item xs="9" className={style.info}>
          <div className={style.space}>
            <label className={style.title}>Tên tài khoản : </label>
            <span className={style.span}>{user.taiKhoan}</span>
          </div>
          <div className={style.space}>
            <label className={style.title}>Họ tên : </label>
            <span className={style.span}> {user.hoTen} </span>
          </div>
          <div className={style.space}>
            <label className={style.title}>Email : </label>
            <span className={style.span}> {user.email}</span>
          </div>
          <div className={style.space}>
            <label className={style.title}>Số điện thoại: </label>
            <span className={style.span}> {user.soDT} </span>
          </div>
          <div className={style.space}>
            <label className={style.title}>Ngày đăng ký: </label>
            <span className={style.span}> *********</span>
          </div>
          <div className={style.space}>
            <label className={style.title}>Thông tin đặt vé: </label>
            {user.thongTinDatVe !== [] ? (
              <span className={style.span}> Đang cập nhật </span>
            ) : (
              <span className={style.span}> {user.thongTinDatVe} </span>
            )}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Profile;
