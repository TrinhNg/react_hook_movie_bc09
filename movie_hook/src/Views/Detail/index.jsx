import { Box, CardMedia, Grid, Typography } from "@material-ui/core";
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovieId } from "../../Store/Action/Movie";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../../Component/Header/index";
import { Rating } from "@material-ui/lab";

const useStyles = makeStyles((theme) => {
  return {
    img: {
      border: "1px solid black",
      width: "400px",
      height: "400px",
    },
    center: {
      maxWidth: "1280px",
      margin: "auto",
      marginTop: theme.spacing(2),
    },
    title: {
      color: "#E885B3",
      marginBottom: theme.spacing(0.7),
      fontStyle: "italic",
    },
    titleDG: {
      color: "#E885B3",
      marginTop: "10px",
      fontStyle: "italic",
      paddingLeft: theme.spacing(11.5),
    },
    span: {
      color: "#5E775D",
    },
    video: {
      marginTop: theme.spacing(1.5),
      height: "250px",
    },
  };
});

const Detail = (props) => {
  const style = useStyles();

  // lấy id từ props
  // console.log("id", props.match.params.id);
  const id = props.match.params.id;

  console.log("props", props.match.params.id);
  const dispatch = useDispatch();

  const moviesID = useSelector((state) => {
    return state.Movie.movieId;
  });

  const getMovieId = useCallback(() => {
    dispatch(fetchMovieId(id));
  }, [id, dispatch]);

  useEffect(() => {
    getMovieId();
  }, [getMovieId, dispatch]);

  const { maPhim, tenPhim, moTa, hinhAnh, sapChieu, trailer, danhGia } =
    moviesID;

  return (
    <>
      <Header />
      <Box marginTop="20px">
        <Typography
          variant="h2"
          align="center"
          style={{ color: "#67AA6C", fontFamily: "Raleway" }}
        >
          THÔNG TIN CỤ THỂ
        </Typography>
        <Grid
          container
          spacing={1}
          direction="row"
          justifyContent="center"
          alignItems="center"
          className={style.center}
        >
          <Grid item xs={6}>
            <CardMedia
              component="img"
              alt="Contemplative Reptile"
              src={hinhAnh}
              title="Contemplative Reptile"
              className={style.img}
            />
            <Typography variant="h6" align="left" className={style.titleDG}>
              Đánh giá:
              <Rating
                name="simple-controlled"
                value={danhGia / 2}
                style={{ marginTop: 10, top: 5, left: 5 }}
              />
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h6" align="left" className={style.title}>
              Tên phim: <span className={style.span}>{tenPhim}</span>
            </Typography>
            <Typography variant="h6" align="left" className={style.title}>
              Mã phim: <span className={style.span}>{maPhim}</span>
            </Typography>
            <Typography variant="h6" align="left" className={style.title}>
              Mô tả : <span className={style.span}>{moTa}</span>
            </Typography>

            <Typography variant="h6" align="left" className={style.title}>
              Thời gian dự kiến:
              <span className={style.span}>
                {sapChieu ? " Sắp ra mắt" : " Chưa ra mắt"}
              </span>
            </Typography>
            <CardMedia
              component="iframe"
              image={trailer}
              autoPlay
              className={style.video}
            />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default Detail;
