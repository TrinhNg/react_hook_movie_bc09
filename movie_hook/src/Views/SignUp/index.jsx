import { TextField, Typography, Button, Container } from "@material-ui/core";
import React from "react";
import Header from "../../Component/Header/index";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { fetchUser } from "../../Store/Action/User";
import { useHistory } from "react-router";
import useStyle from "./style";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("User is required"),
  matKhau: yup
    .string()
    .required("User is required")
    .min(10, "At least ten characters"),
  email: yup.string().required("Email is required").email("Email is invalid"),
  soDT: yup.string().required("Phone is required"),

  maNhom: yup.string().required("Code is required"),
  hoTen: yup.string().required("User is required"),
});

const SignUp = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDT: "",
      maNhom: "",
      hoTen: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formik.values);
    if (!formik.isValid) return;
    dispatch(
      fetchUser(formik.values, () => {
        history.push("/signin");
      })
    );
  };
  const { title, border } = useStyle();
  return (
    <div>
      <Header />
      <Container maxWidth="sm">
        <Typography variant="h3" align="center" className={title}>
          SIGN UP
        </Typography>
        <form onSubmit={handleSubmit} className="w-25 mx-auto">
          <div>
            <TextField
              className={border}
              fullWidth
              name="taiKhoan"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.taiKhoan}
              label="Tài khoản"
              variant="outlined"
            />
            {formik.touched.taiKhoan && (
              <p style={{ color: "red" }}>{formik.errors.taiKhoan}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              fullWidth
              name="matKhau"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.matKhau}
              type="password"
              label="Mật khẩu"
              variant="outlined"
            />
            {formik.touched.matKhau && (
              <p style={{ color: "red" }}>{formik.errors.matKhau}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              fullWidth
              name="email"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.email}
              label="Email"
              variant="outlined"
            />
            {formik.touched.email && (
              <p style={{ color: "red" }}>{formik.errors.email}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              fullWidth
              name="soDT"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.soDT}
              label="Số điện thoại"
              variant="outlined"
            />
            {formik.touched.soDT && (
              <p style={{ color: "red" }}>{formik.errors.soDT}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              fullWidth
              name="maNhom"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.maNhom}
              label="Mã nhóm"
              variant="outlined"
            />
            {formik.touched.maNhom && (
              <p style={{ color: "red" }}>{formik.errors.maNhom}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              fullWidth
              name="hoTen"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.hoTen}
              label="Họ tên"
              variant="outlined"
            />
            {formik.touched.hoTen && (
              <p style={{ color: "red" }}>{formik.errors.hoTen}</p>
            )}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <Button type="submit" variant="contained" color="secondary">
              Sign Up
            </Button>
          </div>
        </form>
      </Container>
    </div>
  );
};

export default SignUp;
