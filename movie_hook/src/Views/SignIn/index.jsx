import { Button, Container, TextField, Typography } from "@material-ui/core";
import React from "react";
import Header from "../../Component/Header";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import * as yup from "yup";
import { fetchSignIn } from "../../Store/Action/User";
import { useHistory } from "react-router";
import useStyle from "../SignUp/style";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("User is required"),
  matKhau: yup.string().required("Pass is required"),
});

const SignIn = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSumit = (e) => {
    e.preventDefault();
    if (!formik.isValid) return;
    dispatch(
      fetchSignIn(formik.values, () => {
        history.push("/");
      })
    );
  };

  const handleSetDefault = () => {
    const defaultplayer = {
      taiKhoan: "con bo",
      matKhau: "0123456789",
    };
    formik.setValues(defaultplayer);
  };

  const { title, border } = useStyle();
  return (
    <div>
      <Header />
      <Container maxWidth="sm">
        <Typography className={title} variant="h3">
          Đăng Nhập
        </Typography>
        <form onSubmit={handleSumit}>
          <div>
            <TextField
              className={border}
              name="taiKhoan"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.taiKhoan}
              fullWidth
              label="Tài khoản"
              variant="outlined"
            />
            {formik.touched.taiKhoan && (
              <p style={{ color: "red" }}>{formik.errors.taiKhoan}</p>
            )}
          </div>
          <div>
            <TextField
              className={border}
              name="matKhau"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.matKhau}
              type="password"
              fullWidth
              label="Mật khẩu"
              variant="outlined"
            />
            {formik.touched.matKhau && (
              <p style={{ color: "red" }}>{formik.errors.matKhau}</p>
            )}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <Button type="submit" variant="contained" color="primary">
              Đăng Nhập
            </Button>
            <Button
              type="button"
              variant="contained"
              color="secondary"
              onClick={handleSetDefault}
            >
              Người dùng mặc định
            </Button>
          </div>
        </form>
      </Container>
    </div>
  );
};

export default SignIn;
