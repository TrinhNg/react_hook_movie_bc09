import React, { useEffect } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import Home from "./Views/Home";
import Detail from "./Views/Detail";
import SignIn from "./Views/SignIn";
import SignUp from "./Views/SignUp";
import { useDispatch } from "react-redux";
import { fetchMe } from "./Store/Action/User";
import Profile from "../src/Views/Profile/Profile";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem("t");
    if (token) {
      dispatch(fetchMe);
    }
  }, []);
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/detail/:id" component={Detail} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
        <Route path="/profile" component={Profile} />
        <Route exact path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
